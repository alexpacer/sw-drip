require 'spec_helper'

describe Account do
  it { should ensure_length_of(:name).is_at_least(5).is_at_most(50) }
  #it { should ensure_length_of(:number).is_at_least(5).is_at_most(50) }


  describe TermDepositAccount do
    it "should create TermDeposit account" do
      #puts "1. creating a new TermDeposit"
      account = create(:term_deposit_account)

      #puts account.inspect

      # account.add_user!(user)
      # account.save!.should eq true
      account.term_deposit_mature_records.length.should > 0
      account.current_balance.should eq account.initial_balance
      
      #    debugstr <<OUTPUT
      #--------Term Deposit account Summary----------
      #Given that you have #{account.initial_balance} for start in "#{account.name}".
      #Interest earned so far: #{account.interest_earned}.
      #Account balance is: #{account.current_balance}.
      #OUTPUT
    end

  end
end
