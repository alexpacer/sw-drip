# I'm expecting these specs to be run in order, so please run rspec with --order default,
# Check out guadfile for this configuration
# Referring to:
# https://github.com/thoughtbot/factory_girl/blob/master/GETTING_STARTED.md
# 
require 'spec_helper' 

describe User do
  it { should allow_value("ABC1234567890").for(:first_name) }
  it { should allow_value("ABC1234567890").for(:last_name) }
  it { should_not allow_value("aaa").for(:email) }
  it { should ensure_length_of(:first_name).is_at_least(5).is_at_most(50) }
  it { should ensure_length_of(:last_name).is_at_least(5).is_at_most(50) }
  it { should ensure_length_of(:password).is_at_least(5).is_at_most(20) }

  
  describe "#create user" do
    usr = User.new()

    it "should create & save user" do  

      # Arrange
      expected_email = "myemail@email.com"

      # Act
      usr.email = expected_email
      usr.password = "password1"
      usr.first_name = "First name"
      usr.last_name = "last name"

      # Asset
      usr.save!.should eq true
      usr.email.should eq expected_email
    end
    
    it "should have default Cash account" do
      usr.accounts.size.should >= 1
      usr.accounts.first.description.should =~ /cash/i
    end
    
  end
  
  describe "#User account management" do
    #let(:user){ Factory(:user) }
    usr = FactoryGirl.build(:user)
    
    it "should not add same account twice" do
      new_acc = FactoryGirl.build(:savings_account)
      usr.add_account(new_acc)
      usr.add_account(new_acc)
      usr.add_account(new_acc)
      usr.add_account(new_acc)
      usr.add_account(new_acc)
      usr.accounts.size.should <= 2
    end
    
    it "should delete single account correctly" do
      new_acc = FactoryGirl.build(:savings_account)
      usr.add_account(new_acc)
      usr.remove_account(new_acc)
      usr.accounts.find(new_acc.id).should eq nil
    end

    it "Should update password" do
      expected_pwd = "!111234"
      usr.password = expected_pwd
      usr.save!

      usr.password.should eq expected_pwd
    end

  end
  
end
