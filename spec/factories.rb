require 'factory_girl'

FactoryGirl.define do
  sequence :account_name do |n| "Account #{n}" end
  sequence :savings_account_name do |n| "Savings Account #{n}" end
  sequence :termdeposit_account_name do |n| "TermDeposit Account #{n}" end
  sequence :first_name do |n| "First#{n}" end
  sequence :last_name do |n| "Last#{n}" end
  sequence :email do |n| "user#{n}@email.com" end

  factory :user, class: User do
    email
    first_name
    last_name
    password "secrect"
  end

  factory :cash do
    name "Cash Account"
    number nil
    description "Cash in hand"
    initial_balance 1000
    currency "TWD"
  end

  factory :savings_account, class: SavingsAccount do
    sequence(:name, 1000){|n| "Savings Account #{n}"}
    sequence(:number, 100000){|n| "#{n}-#{n+1}-#{(0..9999).to_a.sample}"}
    initial_balance [*1000..9000].sample
    currency "TWD"
    interest 1.6
  end

  factory :term_deposit_account, class: TermDepositAccount do
    term_start "2011/06/29"
    term_length 365
    name "New TermDeposit Account"
    number "AAAAAAA"
    description "new Term Deposit Account created manually"
    currency "TWD"
    initial_balance 3000.0
    interest 2.4
  end

end

# A dummy user profile

FactoryGirl.define do
  
  #f.accounts{ |a| [a.association(:default_cash_account)] }
end


# Transaction object
# Factory.define :transaction, :class => Transaction do |f|
#   f.sequence(:description){ |n| "description #{n}" }
#   f.amount [*-100..999].sample
#   # f.association :account, :factory => :default_cash_account
# end


# accounts[Savings]
#
# Factory.define :rspec --order , :class => SavingsAccount do |f|
#   f.name{ |n| n.name = Factory.next(:savings_account_name) }
#   f.sequence(:number){ |n| "#{n}-#{n+1}-#{(0..9999).to_a.sample}" }
#   f.sequence(:description){ |n| "Savings account #{n}" }
#   f.initial_balance [*1000..9000].sample
#   f.currency "TWD"
#   f.interest 1.6
# end

#
# Term Deposit Account
#
# Factory.define :termdeposit_account, :class => TermDepositAccount do |f|
#   f.name{ |n| n.name = Factory.next(:termdeposit_account_name) }
#   f.sequence(:number){ |n| "#{n}-#{n+1}-#{(0..9999).to_a.sample}" }
#   f.sequence(:description){ |n| "TermDeposit account #{n}" }
#   f.initial_balance [*3000..9000].sample
#   f.currency "TWD"
#   f.interest [*1..5].sample + rand()
  
#   f.is_auto_renew true
#   f.term_start "2012/06/29"
#   f.term_length 365
#   #f.is_fix_interest true
#   #f.next_interest nil
# end

