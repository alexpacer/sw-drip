module AccountsHelper
  
  # Produce a drop down selection control to search for accounts
  def h_account_selection(accounts, options={})
    return accounts if accounts.nil?
    arr = []
    accounts.each do |a|
      arr << ["#{a.name} (#{a._type})", a._id]
    end
    select_tag :account, options_for_select(arr), options
  end
  
  # Produce a select_tag html element for account type
  def h_account_type_selection(form, obj, options={})
    selected_string = (obj._type.nil? ? "" : obj._type).gsub(/\s+/, '').strip
    
    form.select :_type,
      options_for_select(Settings.account_types.sort, selected_string), 
      {},
      options
  end
  
  # get account transaction
  
  def account_trans(account)
    return nil if account.nil?
    Transaction.where(:account => account)
  end
  
  # display friendly account type name by giving the shortened
  def h_account_type_name_by_index(index)
    Settings.account_types.each{ |a|
      return a[0] if a[1] == index
    }
  end
  
  # Sum each account balances
  
  def account_balance_sum(accounts)
    sum = 0
    accounts.each{|a| sum += a.current_balance }
    sum
  end
  
  
  ##
  ## Page view related helper methods
  ##
  

  
end