module ApplicationHelper
  
  # takes a number and options hash and outputs a string in any currency format
  # :currency_before => false puts the currency symbol after the number
  # default format: $12,345,678.91  
  def currencify(number, options={})  
    options = {:currency_symbol => "$", :delimiter => ",", :decimal_symbol => ".", :currency_before => true, :currency_name => '', :show_operator => true }.merge(options)
    
    operator = (number ||= 0) >= 0 ? '+' : '-'
    operator_css_name = (operator == '+') ? 'positive' : 'negative'
    if !options[:show_operator] then operator = '' end
    
    # split integer and fractional parts, also convert neg to positive number
    int, frac = ("%.2f" % number.abs).split('.')
    # insert the delimiters
    int.gsub!(/(\d)(?=(\d\d\d)+(?!\d))/, "\\1#{options[:delimiter]}")
    
    if options[:currency_before]
      html_wrapper(:span, "currency_#{operator_css_name}") do
        options[:currency_name] + " #{operator}" + options[:currency_symbol] + int + options[:decimal_symbol] + frac
      end
    else
      html_wrapper(:span, "currency_#{operator_css_name}") do
        options[:currency_name] + " #{operator}" + int + options[:decimal_symbol] + frac + options[:currency_symbol]
      end
    end
  end
  
  
  # Returns "current" string if the item string matches the compare string.
  # Might want to use this method to hightlight the current item in a menu list.  
  def current_item?(item, compare, ignore_case = true)
    i = ignore_case ? item.downcase() : item
    c = ignore_case ? compare.downcase() : compare
    (i == c) ? "current" : ""
  end
  
  
  # Takes a datetime object and covert to a more friendly datetime formate to display on page  
  def datefy(tt)
    content_tag(:span, :class =>'datetime') do
       tt.strftime("%B %d, %Y <span class='weekday'>(%a)</span>").html_safe()
    end
  end
  
  # Wraps the content into a specified html tag  
  def html_wrapper(tag_name, class_name, &block)
    content = capture(&block).html_safe
    content_tag(tag_name, content, :class => class_name).html_safe
  end
  

  def h_action_dispatcher(action, &block)
    if @current_action.downcase == action.downcase
      yield
    end
  end

  # retrieve the display name of the given account key
  def account_type_display_name(key)
    Settings.account_types.select{|index,val|val==key}.first[1]
  end
  
end