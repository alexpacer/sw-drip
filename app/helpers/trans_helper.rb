module TransHelper

  # Generates a dropdown selection list of, basically + and - signs for user to choose the kinda operation
  # of the given transaction 
  def h_trans_op_dropdown(amount, options={})
    op_4_select = options_for_select(['-', '+'], (amount.nil? || amount < 0) ? '-' : '+') 
    select_tag :operation, op_4_select, options
  end
  
end