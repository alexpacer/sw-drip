# A savings account WIKI[http://en.wikipedia.org/wiki/Savings_account] is a basic account
# that allows you to access your money freely but with very little interest rate
class SavingsAccount < Account
  
  # Calculate the total interest grown based on the interest rate 
  # and the created date of this savings account
  # * You can specify :from & :to parameters
  # * Calculation is based on days
  def interest_growth(args = {})
    args[:from] ||= created_at.to_date
    args[:to]   ||= Time.now.to_date
    days_between = args[:to] - args[:from]
    
    (current_balance * (interest / 100) * days_between) / 365
  end
  
end