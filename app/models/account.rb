class Account
  include Mongoid::Document
  include Mongoid::Timestamps
  validates_presence_of :name, :message => "account name is required."
  validates_length_of :name, in: 5..50

  field :name, type: String
  field :number, type: String
  field :description, type: String
  field :initial_balance, type: BigDecimal, :default => 0.0
  field :current_balance, type: BigDecimal, :default => 0.0

  field :interest, type: BigDecimal, :default => 0.0
  field :currency, type: String
  
  belongs_to :user

  attr_accessor :_type

  before_create do
    self.current_balance = initial_balance
  end

  def transactions
    Transaction.where(:account_id => self.id)
  end

  def update_current_balance(amount)
    self.current_balance += amount
    self.save!
  end

  
  def self.owned_by(user)
    Account.where(:user_id => user.id)
  end

  # def change_type(new_type)
  #   db = Mongoid::Sessions.default
  #   hs = Hash[self.attributes]
  #   hs['_type'] = new_type

  #   puts hs.inspect

  #   #db[:accounts].find(_id: self.id).update(hs)
  # end

end

