class User
  include Mongoid::Document
  include Mongoid::Timestamps
  validates_length_of :first_name, maximum: 50
  validates_length_of :last_name, maximum: 50
  validates_uniqueness_of :email
  validates_presence_of :email, :message => "email is required"
  validates_presence_of :password, :message => "password is required"
  validates_length_of :password, in: 5..20
  validates_format_of :email, :with => /\A.+@.+\z/, :message => "email format is not valid"
  validates_confirmation_of :password, :message => "password should match the confirmation"

  field :email, type: String
  field :password, type: String
  field :first_name, type: String
  field :last_name, type: String

  has_many :accounts

  after_create do
    acc = Cash.new(:name => "Cash in hand", :description => "Your cash here")
    accounts << acc
    self.save!
  end

  # # Adds account to this user. While doing so, it will call add_user! method
  # # of the account object being added to associate the user to that account
  def add_account(acc)
    if !accounts.find(acc.id)
      accounts << acc
      save!
    end
  end

  # # Removes the account association from this user
  def remove_account(acc)
    accounts.delete(acc)
    save!
  end

end
