# A transaction should belongs to one account, and belongs to one user
class Transaction
  include Mongoid::Document
  include Mongoid::Timestamps
  validates_numericality_of :amount, :message => "amount should be numerical"
  
  after_save :update_account_balance
  before_update :save_amount_before_update
  
  field :description, type: String, :default => ''
  field :amount, type: BigDecimal
  
  # Associations  
  belongs_to :account
  belongs_to :user
  
  # Class variables  
  @@original_amount=0
   
  # Queries the transaction table that is done by particular user
  
  def self.done_by(user)
    Transaction.where(:user_id => user.id)
  end  
  
    
  private
    
    # Perform this action after transaction bing updated
    def update_account_balance
      diff = self.amount
      if @@original_amount != 0 && @@original_amount != self.amount
        #
        # workout the difference between new and old amount
        @@original_amount -= self.amount
        if self.amount > @@original_amount
          diff = self.amount - @@original_amount
        else
          diff = @@original_amount - self.amount
        end
      end
      
      account.update_current_balance(diff)
    end

    
    # Saves the original amount before being replaced
    
    def save_amount_before_update
      @@original_amount = self.amount
    end
  
end