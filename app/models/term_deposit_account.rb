# TermDepositAccount as defined in WIKI[http://en.wikipedia.org/wiki/Time_deposit] is an account that
# develop interest in a fixed term. every term has it's fix interest based on the amount of the account.
# The interest earned will be granted after each term is matured, user should be able to choose
# whether they want to renew the account with new (or same) term when current term matures.
class TermDepositAccount < Account
  # the start date of this current term
  field :term_start, type: Date
  
  # in _DAYS_ of each term will last
  field :term_length, type: Integer
  
  # Number of days that the system will notify user to update
  # the account's maturity command. 
  field :renew_notification_period, type: Integer
  
  # Prepare for next term if its specified
  field :is_auto_renew, type: Boolean
  field :next_term_start, type: Date
  field :next_term_length, type: Integer
  field :next_term_interest, type: BigDecimal
  
  # A term deposit account would have many mature records saved
  # these records will be used to calculate how much interest has been
  # granted from this account
  embeds_many :term_deposit_mature_records
  
  after_save :update_current_balance
  
  # Calculate the total interest grown based on the interest rate 
  # and the created date of this savings account
  # * You can specify :from & :to parameters
  # * Calculation is based on days
  
  def interest_growth(args = {})
    args[:from] ||= term_start
    args[:to]   ||= term_start.days_after(term_length)
    
    days_between = args[:to] - args[:from]
    
    ((initial_balance + interest_earned) * (interest / 100) * days_between) / 365
  end
  
  
  # Calcuates number of interest earned (from all matured record)
  
  def interest_earned
    sum = 0
    term_deposit_mature_records.each do |mr|
      sum += mr.earned
    end
    sum
  end
  
  
  # Calculates the current balance of the account, this method will determine that
  # Whether if a term is matured or not. 
  
  def update_current_balance
    matured?
  end
  
  
  # Check if the current term is matured(by using specified :til parameter or current system date),
  # If term is matured when this method is called, interest earned for the term will be saved into
  # a new TermDepositMatureRecord record.
  # * if til date is not given it will be the current system date
  # * account is decided not auto renewing (stalled) if it does not have :term_start attribute
  # * if the current term is matured:
  #   - a TermDepositMatureRecord will be created as embedded document in :term_deposit_mature_records
  #   - a new :term_start date will be assigned to the next day of mature day if :is_auto_renew is switched on. 
  #   - :term_start will be set to nil of :is_auto_renew is turned off.
  def matured?(til = Time.now.utc.to_date)
    if !term_start.nil?
      mature_date = (self[:term_start] + self[:term_length]).to_date
      if mature_date > til
        return false
      else
        # term deposit is matured, put the amout of interest earned in db and calculate a new balance
        days_between = mature_date.to_date - term_start
        
        mature_record = TermDepositMatureRecord.new(
          :start => term_start, 
          :mature => mature_date,
          :interest => interest, 
          :earned => ((initial_balance + interest_earned) * (interest / 100) / 365) * days_between
        )
        term_deposit_mature_records << mature_record
        
        if !is_auto_renew.nil? && is_auto_renew
          self[:term_start] = mature_date
        else
          if self[:next_term_start].nil?
            self[:term_start] = nil
          else
            self[:term_start] = self[:next_term_start]
            self[:term_length] = self[:next_term_length]
            self[:interest] = self[:next_term_interest].to_f
          end
        end
        
        self[:current_balance] = initial_balance + interest_earned
        
        save!

        return true
      end
    end
  end
  
end

