# The mature record represents each history record of matured term of term deposit(s). 
class TermDepositMatureRecord
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :start, type: Date
  field :mature, type: Date
  field :earned, type: BigDecimal
  field :interest, type: BigDecimal
  
  embedded_in :term_deposit_account
  
end