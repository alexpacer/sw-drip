# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

User =
  init: ->
    $("[data-js='modal-edit-transaction']").popModal()
    $("[data-js='modal-add-transaction']").popModal()

jQuery ->
  $(document).ready ->
    User.init()
