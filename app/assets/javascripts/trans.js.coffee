# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

#= require "jstimezonedetect/jstz"
#= require "underscore/underscore"
#= require "bootstrap-calendar/language/fr-FR"
#= require "bootstrap-calendar/calendar"
#= require_self

$ ->
  trans =
    init: ->
      $("#add_transaction").hide()
      #$(window).bind("resize", trans.rescale())
      # Resize modal dialog
      # $("#modal-dialog").on "show", (e)->
      #   console.log $(this).find(".modal-body")
      #   $(this).find(".modal-body").css
      #     width: "auto"
      #     height: "auto"
      #     "max-height": "100%"
  
      
    initCalendar: ->
      if($("#calendar").length > 0)
        # Initialize calendar
        calendar = $("#calendar").calendar
          events_source: $("#calendar").attr("data-event-url")
          view: "month"
          tmpl_path: "/tmpls/"
          tmpl_cache: false
          modal: "#events-modal"
    # rescale: ->
    #   size = {width:$(window).width(), height:$(window).height()}
    #   offset = 20
    #   offsetBody = 150
    #   $("#modal-dialog").css("height", size.height - offset)
    #   $(".modal-body").css("height", size.height - (offset + offsetBody))
    #   $("#modal-dialog").css("top", 0)
  
  # $(document).ready ->
  #   trans.init()

  # This init will be called when calendar finished loading
  $(document).on "page:load", 
    #trans.init()
    trans.initCalendar()