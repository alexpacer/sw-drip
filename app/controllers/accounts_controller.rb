class AccountsController < ApplicationController
  before_filter :require_user_login
  layout 'user'

  def index
    @accounts = @user.accounts
  end

  def new
    @account = Account.new()
  end

  def edit
    @account = Account.find(params[:id])
  end

  def update
    @account = Account.find(params[:id])
    
    respond_to do |f|
      if @account.update_attributes(params[:account])
        flash[:notice] = "Account updated successfully"
        f.html{ redirect_to accounts_path(), :recent => @account  }
      else
        flash[:error] = "Failed to update account"
        f.html{ render 'edit' }
      end
    end
  end
  
  def destroy
    @account = Account.find(params[:id])
    respond_to do |f|
      if @account.destroy
        flash[:notice] = 'Account deleted successfully'
      else
        flash[:error] = "Was unable to delete the specified account"
      end
      f.html{ redirect_to accounts_path, :recent => @account }
    end
  end
  
  
  def create
    if params[:account][:_type] == 'SavingsAccount'
      @account = SavingsAccount.new(params[:account])
    elsif params[:account][:_type] == 'TermDepositAccount'
      @account = TermDepositAccount.new(params[:account])
    elsif params[:account][:_type] == 'Cash'
      @account = Cash.new(params[:account])
    end

    respond_to do |f|
      if @account.save
        @user.add_account(@account)
        flash[:notice] = "Account created successfully"
        f.html{ redirect_to accounts_path, :recent => @account }
      else
        flash[:error] = @account.errors.full_messages
        f.html{ render 'new' }
      end
    end
  end
  
  def show
    @account = Account.find(params[:id]) || err_notfound
  end
  

end
