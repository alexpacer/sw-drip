class TransController < ApplicationController
  before_filter :require_user_login
  respond_to :json
  layout 'user'

  # Display a quick form to create transaction,
  # The page should take paramenter[:account] to have the account preselected in dropdown list
  def new
    @account = params[:account].nil? ? nil : Account.find(params[:account])
    @accounts = Account.owned_by(@user)
    @new_transaction = Transaction.new()

    respond_to do |f|
      f.html{ render :layout=>nil }
    end
  end
  
  # Creates a transaction record
  def create
    @trans = Transaction.new(params[:tran])
    @fromacc = Account.find(params[:account])
    
    # assume that input are all nutual numbers, we use operation param to 
    # determine the number
    amount = (params[:operation] == '-') ? -BigDecimal.new(@trans.amount) : BigDecimal.new(@trans.amount)
    @trans.amount = amount
    
    @trans.account = @fromacc
    @trans.user = @user
    
    respond_to do |f|
      if @trans.save!
        msg = "Transaction created"
        flash[:notice] = msg
        f.html{ redirect_to user_dashboard_path, :recent => @trans }
        f.json{ render json: { :status => :success, :message => msg, :detail => @trans.as_json } }
      else
        msg = "failed to create transaction"
        flash[:notice] = msg
        f.html{ render user_dashboard_path, :recent => @trans }
        f.json{ render json: {:status => :error, :message => msg } }
      end
    end
  end
  
  
  # Destroy a transaction record 
  def destroy
    @trans = Transaction.find(params[:id])
    
    respond_to do |f|
      if @trans.destroy
        flash[:notice] = 'Transaction deleted'
        f.html{ redirect_to user_dashboard_path }
      else
        flash[:notice] = 'Failed to delete transaction'
        f.html{ render user_dashboard_path, :recent => @trans }
      end
    end
  end
  
  # AKA Daily transactions
  def index
    @default_days = 7
    account_id = params[:account]
    begin
      @days = Integer(params[:days], 10)
    rescue
      @days = @default_days
    end
    
    tquery = Transaction.done_by(@user)
      .where(:created_at.gte => @days.days.ago)
    
    if(!account_id.nil?)
      tquery = tquery.where(:account_id => account_id)
    end
    
    @trans = tquery.sort(:created_at.desc)
    @total_expenses = @trans.collect{|m| m.amount }.inject{|sum, t| sum + t }
    @total_expenses = 0 if @total_expenses.nil?
  end
  
  
  # Shows a calendar view of the expenses
  def calendar
    today = params[:f] ? params[:f] : Time.now.utc.to_date
    @new_transaction = Transaction.new()
  end
  
  
  # Provide a json calendar events feed to be consumed by calendar
  def calendarevents
    operator = (params[:op] == '+') ? '+' : '-'
    start = params[:start] ? DateTime.strptime(params[:start],'%s') : DateTime.new(Date.today.year, Date.today.month, 1)
    to = params[:end] ? DateTime.strptime(params[:end], '%s') : DateTime.civil(Date.today.year, Date.today.month, -1)
    
    start = Time.utc(start.year, start.month, start.day)
    to = Time.utc(to.year, to.month, to.day)
    
    events = Transaction.done_by(@user).where(:created_at.gte => start).where(:created_at.lt => to)
    if(operator == '+')
      events = events.where(:amount.gte => 0)
    else
      events = events.where(:amount.lte => 0)
    end
    
    respond_to do |f|

      json = {
          :success => 1,
          :result => events.all.map!{|e| 
            {
              :id=>e._id.to_s,
              :title=>"#{e.description} [#{e.amount}]",
              :url=>url_for(:controller=>"trans",:action=>"show",:id=>e._id.to_s),
              :class=>(e.amount>=0) ? "event-info" : "event-warning",
              :start=>e.created_at.to_time.to_i*1000,
              :end=>(e.created_at.to_time.to_i+60)*1000
            }
          }
      }

      f.html{ render :json => json }
      f.json{ render :json => json }
    end

  end
  
  # reviewing single transaction
  def show
    @trans = Transaction.find(params[:id])

    respond_to do |f|
      if @trans.nil?
        flash[:error] = 'Failed to find the transaction to edit'
        f.html{ }
      else
        f.html{ render :layout=>"application" }
      end
    end
  end
  
  # trying to edit a transaction
  def edit
    @trans = Transaction.find(params[:id])
    
    respond_to do |f|
      if @trans.nil?
        flash[:error] = 'Failed to find the transaction to edit'
        f.html{ redirect_to user_dashboard_path }
      else
        f.html{ render :layout=>nil }
      end
    end
  end
  
  # Trying to update a transaction
  def update
    @trans = Transaction.find(params[:id])
    params[:tran][:amount] = (params[:operation] == '-') ? -(params[:tran][:amount].to_f) : params[:tran][:amount]
    
    respond_to do |f|
      if @trans.update_attributes(params[:tran])
        flash[:notice] = 'Transaction updated successfully'
        f.html{ redirect_to trans_path }
      else
        flash[:notice] = 'Failed to update transaction'
        f.html{ render edit_tran(@trans), :recent => @trans }
      end
    end
  end
  
  
end