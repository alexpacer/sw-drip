class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  private
    before_filter :instantiate_controller_and_action_names, :init_page_elements

  # use this method to:
  #  1. assign user session in parameter
  #  2. retreive user session if no prameter is given
  #     this method will check if there's a current user session, will return
  #     the user object if it's there,
  def current_user?(user=nil, enable_cookie=false)
    if !user.nil?
      session[:u] = user.id.to_s
      cookies[:u] = user.id.to_s if enable_cookie
    end
    
    usr = enable_cookie ? cookies[:u] : session[:u]
    usr.nil? ? false : User.find(usr)
  end
  
  
  # Initialize @user variable when there's an user session exists. otherwise
  # request user to login  
  def require_user_login
    unless current_user?
      flash[:error] = "Please login before proceed"
      redirect_to user_login_path
    else
      @user = current_user?
    end
  end
  
  # Instantiate controller and action names so it is accessable in views
  def instantiate_controller_and_action_names
    @current_action = action_name
    @current_controller = controller_name
  end

  def init_page_elements
    @my_accounts = current_user? ? current_user?.accounts : nil
  end
  

  # should request this method when given resource is not found in repository
  def err_notfound
    raise ActionController::RoutingError.new('Resource not found')
  end

end
