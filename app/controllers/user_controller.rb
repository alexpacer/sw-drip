class UserController < ApplicationController
  before_filter :require_user_login, :except => [:register, :landing, :login]
  layout 'user'
  
  def index
    redirect_to :user_dashboard
  end

  def dashboard
    @accounts = @user.accounts
    @new_transaction = Transaction.new()
  end

  def profile
    
  end
  
  # Change a user's password 
  def change_pwd
    @user = current_user?
    old_password = params[:old_password]
    new_password = params[:new_password]
    new_password_confirm = params[:confirm_password]
    
    respond_to do |f|      
      if is_valid?(@user.userid, old_password)
        @user.password = new_password
        
        if @user.save
          flash[:notice] = "Password changed successfully"
          f.html{ redirect_to user_profile_path }
        else
          flash[:error] = "Unable to change your password"
          f.html{ redirect_to user_profile_path }
        end
      else
        flash[:error] = "Unable to verify your existing password!"
        f.html{ redirect_to user_profile_path }
      end
    end
  end

  # display Registration page
  
  def register
    respond_to do |f|
      if request.post?        
        @user = User.new(params[:user])
        if @user.save
          current_user?(@user)
          f.html{ redirect_to user_landing_path }
        else
          flash[:error] = @user.errors.full_messages
          f.html{ render 'register', :layout => 'application', errors: @user.errors.full_messages }
        end
      else
        if current_user?
          flash[:error] = "Seems like you are already logged on, please logout first."
          f.html{ redirect_to user_dashboard_path }
        end
        
        @user = User.new()
        f.html{ render 'register', :layout => 'application' }
      end
    end
  end
  
  # the 1st page a successfully registered user enters
  def landing
    @user = current_user?
  end
  
  
  def login
    respond_to do |f|
      if request.post?
        email = params[:user][:email]
        passwd = params[:user][:password]
        kalive = (params["keep-alive"]  == "on")

        @user = is_valid?(email, passwd)
        
        if !@user.nil?
          current_user?(@user, kalive)
          f.html{ redirect_to user_dashboard_path }
        else
          flash[:error] = 'Wrong username or password.'
          f.html{ render 'login', :layout => 'application' }
        end
      else
        f.html{ render 'login', :layout => 'application' }
      end
    end
  end
  
  def logout
    session.delete :logined_user
    redirect_to root_path
  end
  
  
  def is_valid?(email, passwd)
    User.where(:email => email, :password => passwd).first
  end
    
end